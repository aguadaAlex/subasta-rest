import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OfertaTrabajo } from '../models/oferta-trabajo';
import { UpdateOferta } from '../models/update-oferta';

@Injectable({
  providedIn: 'root'
})
export class OfertaTrabajoService {
  ofertaURL = '/api/ofert';

  constructor(private httpClient: HttpClient) { }

  public lista(): Observable<OfertaTrabajo[]> {
    return this.httpClient.get<OfertaTrabajo[]>(this.ofertaURL);
  }
  public detail(id: number): Observable<OfertaTrabajo> {
    return this.httpClient.get<OfertaTrabajo>(this.ofertaURL + `/byoffert/${id}`);
  }
  public save(oferta: OfertaTrabajo): Observable<any> {
    return this.httpClient.post<any>(this.ofertaURL, oferta);
  }
  public update(id: number, oferta: UpdateOferta): Observable<any> {
    return this.httpClient.put<any>(this.ofertaURL + `/${id}`, oferta);
  }
  public delete(id: number): Observable<any> {
    return this.httpClient.delete<any>(this.ofertaURL + `/${id}`);
  }

}
