import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { OfertaTrabajo } from '../models/oferta-trabajo';
import { OfertaTrabajoService } from '../service/oferta-trabajo.service';
import { TokenService } from '../service/token.service';

@Component({
  selector: 'app-nueva-oferta',
  templateUrl: './nueva-oferta.component.html',
  styleUrls: ['./nueva-oferta.component.css']
})
export class NuevaOfertaComponent implements OnInit {

  isLogged = false;
  isLoginFail = false;
  errMsj:string;
  nombreUsuario='';
  titulo='';
  tipoTrabajo='';
  descripcionTarea='';
  descripcionZona='';
  fecha='';


  constructor(
    private tokenService:TokenService,
    private ofertaService: OfertaTrabajoService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
  }
  onCreate(): void {
    this.nombreUsuario=this.tokenService.getUserName();
    const oferta = new OfertaTrabajo(this.nombreUsuario,this.titulo, this.tipoTrabajo,this.descripcionTarea,this.fecha);
    console.log(oferta);
    this.ofertaService.save(oferta).subscribe(
      data => {
        this.toastr.success('Oferta Creada', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/listaOfertas']);
      },
      err=>{
        this.isLogged=false;
        this.isLoginFail=true;
        this.errMsj=err.error.message;
        console.log(err.error.message);
      }
    );
  }


}
