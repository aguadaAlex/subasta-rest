import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { OfertaTrabajo } from '../models/oferta-trabajo';
import { UpdateOferta } from '../models/update-oferta';
import { OfertaTrabajoService } from '../service/oferta-trabajo.service';

@Component({
  selector: 'app-editar-oferta',
  templateUrl: './editar-oferta.component.html',
  styleUrls: ['./editar-oferta.component.css']
})
export class EditarOfertaComponent implements OnInit {
  isLogged = false;
  isLoginFail = false;
  errMsj:string;

  oferta: OfertaTrabajo = null;

  constructor(private ofertaService: OfertaTrabajoService,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router) { }

  ngOnInit() {
    const id = this.activatedRoute.snapshot.params.id;
    this.ofertaService.detail(id).subscribe(
      data => {
        this.oferta = data;
      },
      err => {
        this.toastr.error(err.error.mensaje, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
        this.router.navigate(['/']);
      }
    );
  }

  onUpdate(): void {
    const id = this.activatedRoute.snapshot.params.id;
    const oferta = new UpdateOferta(this.oferta.titulo, this.oferta.tipotrabajo,this.oferta.descripcionTarea,this.oferta.fecha);
    console.log(oferta);
    this.ofertaService.update(id,oferta).subscribe(
      data => {
        this.toastr.success('oferta Actualizado', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.router.navigate(['/listaOfertas']);
      },
      err => {
        this.isLogged=false;
        this.isLoginFail=true;
        this.errMsj=err.error.message;
        console.log(err.error.message);
      }
    );
  }

}
