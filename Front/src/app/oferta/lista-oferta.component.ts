import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { OfertaTrabajo } from '../models/oferta-trabajo';
import { OfertaTrabajoService } from '../service/oferta-trabajo.service';

@Component({
  selector: 'app-lista-oferta',
  templateUrl: './lista-oferta.component.html',
  styleUrls: ['./lista-oferta.component.css']
})
export class ListaOfertaComponent implements OnInit {
  ofertas: OfertaTrabajo[] = [];

  constructor(
    private ofertaService: OfertaTrabajoService,
    private toastr: ToastrService
    ) { }

  ngOnInit() {
    this.cargarProductos();
    console.log('chido');
  }

  cargarProductos(): void {
    this.ofertaService.lista().subscribe(
      data => {
        this.ofertas = data;
        console.log(data);
      },
      err => {
        console.log(err);
      }
    );
  }

  borrar(id: number) {
    this.ofertaService.delete(id).subscribe(
      data => {
        this.toastr.success('oferta Eliminada', 'OK', {
          timeOut: 3000, positionClass: 'toast-top-center'
        });
        this.cargarProductos();
      },
      err => {
        this.toastr.error(err.error.message, 'Fail', {
          timeOut: 3000,  positionClass: 'toast-top-center',
        });
      }
    );
  }

}
