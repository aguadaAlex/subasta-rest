export class OfertaTrabajo {
  id?: number;
  username: string;
  titulo: string;
  tipoDeTrabajo: string;
  solicitudes: string[];
  descripcionZona: string;
  descripcionDeTarea: string;
  fecha: string;
  nombreUser:string;

  constructor(nombreUser: string, titulo: string,tipoDeTrabajo: string,descripcionDeTarea:string,fecha:string) {
    this.nombreUser = nombreUser;
    this.titulo = titulo;
    this.tipoDeTrabajo = tipoDeTrabajo;
    this.descripcionDeTarea = descripcionDeTarea;
    this.fecha = fecha;
}

}
