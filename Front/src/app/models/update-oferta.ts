export class UpdateOferta {
  id?: number;
  username: string;
  titulo: string;
  tipoDeTrabajo: string;
  solicitudes: string[];
  descripcionZona: string;
  descripcionDeTarea: string;
  fecha: string;
  nombreUser:string;

  constructor(titulo: string,tipoDeTrabajo: string,descripcionDeTarea:string,fecha:string) {
    this.titulo = titulo;
    this.tipoDeTrabajo = tipoDeTrabajo;
    this.descripcionDeTarea = descripcionDeTarea;
    this.fecha = fecha;
}
}
