package com.javaweb.employservice.service;

import com.javaweb.employservice.dto.CreateOffertRequest;
import com.javaweb.employservice.dto.OffertRequest;
import com.javaweb.employservice.dto.UpdateOfertaRequest;
import com.javaweb.employservice.entity.OfferWork;
import com.javaweb.employservice.entity.Work;
import com.javaweb.employservice.repository.OffertWorkRepository;
import com.javaweb.employservice.repository.WorkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class OffertWorkService {
    @Autowired
    OffertWorkRepository offertWorkRepository;
    @Autowired
    WorkRepository workRepository;
    public Boolean existeUserName(String username){
        return offertWorkRepository.existsByUsername(username);

    }

    public List<OfferWork>getAll(){
        return offertWorkRepository.findAll();

    }
    public List<OfferWork>getOfertByUserName(String name){
        List<OfferWork> ofertas= offertWorkRepository.findByUsername(name);
        return ofertas;

    }

    public OfferWork getOfertById(String id){
        return offertWorkRepository.findById(id).orElse(null);
    }

    public OfferWork save(OffertRequest request){
        //String dt = "2008-01-01";
        //Date dt = new Date();
        OfferWork oferta=new OfferWork();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dt = sdf.format(new Date());//DIA DE HOY
        try{
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(dt));
        c.add(Calendar.DATE, 5);  // SUMO 5 DIAS
        dt = sdf.format(c.getTime());  // dt ES AHORA LA NUEVA FECHA
        System.out.println("Tomorrow: "+dt);
        Date date1= sdf.parse(dt);
            System.out.println("date1:"+sdf.format(date1));
        Date date2=sdf.parse(request.getFecha());
            System.out.println("date2:"+sdf.format(date2));



        OfferWork offerWork = new OfferWork(request.getNombreUser(),
                request.getTitulo(),
                request.getTipoDeTrabajo(),
                request.getDescripcionDeTarea(),
                request.getFecha());


         if (date1.after(date2)){// ACA SE AGREGA LA CONDICION PARA EVALUAR FECHA DATE2 MAYOR A DATE1
                oferta=null;
            }
         else{
             oferta= offertWorkRepository.save(offerWork);
         }

        }
        catch (ParseException except){
            except.printStackTrace();
        }
        return oferta;
    }

    public OfferWork updateOferta(OfferWork oferta, UpdateOfertaRequest request){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dt = sdf.format(new Date());//DIA DE HOY
        try{
            Calendar c = Calendar.getInstance();
            c.setTime(sdf.parse(dt));
            c.add(Calendar.DATE, 5);  // SUMO 5 DIAS
            dt = sdf.format(c.getTime());  // dt ES AHORA LA NUEVA FECHA
            System.out.println("Tomorrow: "+dt);
            Date date1= sdf.parse(dt);
            System.out.println("date1:"+sdf.format(date1));
            Date date2=sdf.parse(request.getFecha());
            System.out.println("date2:"+sdf.format(date2));
        //oferta.setUsername(request.getNombreUser());
            if (date1.after(date2)){// ACA SE AGREGA LA CONDICION PARA EVALUAR FECHA DATE2 MAYOR A DATE1
                System.out.println("entrada uno");
                oferta=null;
            }
            else{
                System.out.println("entrada dos");
                oferta.setTitulo(request.getTitulo());
                oferta.setTipotrabajo(request.getTipoDeTrabajo());
                oferta.setDescripcionTarea(request.getDescripcionDeTarea());
                oferta.setFecha(request.getFecha());
                System.out.println(oferta);
                offertWorkRepository.save(oferta);
            }
        }
        catch (ParseException except){
            except.printStackTrace();
            oferta=null;
        }
        System.out.println("resultado real");
        System.out.println(oferta);
        return oferta;

    }
    public void borrarOferta(String id){
        offertWorkRepository.deleteById(id);
    }

}
